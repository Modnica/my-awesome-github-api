name := "my-awesome-github-api"

version := "0.1"

scalaVersion := "2.13.2"

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")

val circeVersion = "0.12.3"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "2.1.1",
  "org.typelevel" %% "cats-effect" % "2.1.3",

  "com.softwaremill.sttp.client" %% "core" % "2.1.2",
  "com.softwaremill.sttp.client" %% "async-http-client-backend-cats" % "2.1.2",

  "io.circe" %% "circe-core"    % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser"  % circeVersion,

  "org.tpolecat" %% "doobie-core" % "0.8.8",
)

