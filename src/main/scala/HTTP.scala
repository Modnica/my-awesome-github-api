import io.circe._

import sttp.client._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._


case class User(name: String)
case class Repo(user: String, name: String, starsCount: Int)


class HTTP{
  def getMaxStars(username: String):Int =
    getRepos(username).map(x => x.starsCount).max

  implicit val backend: SttpBackend[Identity, Nothing, NothingT] = HttpURLConnectionBackend()

  def getFollowers(username: String): List[User] ={

    val getFollowers = basicRequest.get(uri"https://api.github.com/users/$username/followers")

    implicit val decoderRepo: Decoder[User] = (hCursor: HCursor) =>
      for {
        name <- hCursor.get[String]("login")
      } yield User(name)

    getFollowers.send().body.map(decode[List[User]]) match {
      case Right(value) => value match {
        case Right(value1) => value1
      }
    }
  }

  def getRepos(username: String): List[Repo] = {

    val getRepos = basicRequest.get(uri"https://api.github.com/users/$username/repos")

    implicit val decoderRepo: Decoder[Repo] = (hCursor: HCursor) =>
      for {
        name <- hCursor.get[String]("name")
        starsCount <- hCursor.downField("stargazers_count").as[Int]
      } yield Repo(username, name, starsCount)

    getRepos.send().body.map(decode[List[Repo]]) match {
      case Right(value) => value match {
        case Right(repos) => repos
      }
    }
  }

  def chooseBestRepos(list: List[Repo], goal: Int): List[Repo] =
    list.filter(repo => repo.starsCount > goal)
}