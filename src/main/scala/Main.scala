object Main extends App {
    val http = new HTTP
    val user = "assembledadam"
    val maxStars = http.getMaxStars(user)
    val res = http.getFollowers(user).foldLeft(List[Repo]()){
        case (acc, item) => http.chooseBestRepos(http.getRepos(item.name), maxStars) ++ acc
    }
    println(res)
}